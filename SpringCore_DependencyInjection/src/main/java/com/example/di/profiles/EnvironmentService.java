package com.example.di.profiles;

public interface EnvironmentService {

	public String getEnvironment();
}
