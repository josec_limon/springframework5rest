package com.example.di.aop;

import org.aspectj.lang.annotation.Pointcut;

public class PointcutExample {

	//@Pointcut("execution(* com.example.di.aop.TargetObject.*(..))")
	//@Pointcut("within(com.example.di.aop.*)")
	//@Pointcut("within(com.example.di.aop.TargetObject)")
	@Pointcut("@annotation(Devs4jAnnotation)")
	public void targetObjectMethods() {
		
	}
}
