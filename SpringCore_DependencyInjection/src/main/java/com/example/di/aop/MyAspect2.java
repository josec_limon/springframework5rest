package com.example.di.aop;

import java.lang.reflect.Modifier;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(0)
public class MyAspect2 {

	private static final Logger log = LoggerFactory.getLogger(MyAspect.class);

	// Método que se ejecutará durante el Advice
	// Este método interceptará el metodo hello de TargetObject y se ejecutará antes del target
	//@Before("execution(* com.example.di.aop.TargetObject.hello(..))")
	@Before("PointcutExample.targetObjectMethods()")
	public void before(JoinPoint joinPoint) {
		log.info("Aspecto 2");
		log.info("-------------------------------------");
		
		Signature signature =joinPoint.getSignature();
		log.info("Modificadores: {}", signature.getModifiers());
		log.info("Nombre: {}", signature.getName());
		log.info("Tipo devuelto: {}", signature.getDeclaringTypeName());
		log.info("Argumentos: {}", joinPoint.getArgs());
		log.info("Es publico: {}", Modifier.isPublic(joinPoint.getSignature().getModifiers()));
		
		log.info("Before advice");
	}
}
