package com.example.di.scopes;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
//@Scope("prototype")  //Cada vez que se invoca se crea una nueva instancia
@Scope("singleton")		//Se crea una unica instancia independientemente de las veces que se invoca. 
public class EjemploScopeService {

	
}
