package com.example.di.autowire;

public interface Figure {

	double calcularArea();
}
