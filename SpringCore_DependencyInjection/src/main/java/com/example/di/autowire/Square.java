package com.example.di.autowire;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Square implements Figure{

	// @Value("2.5")
	//  Para cargar los valores a traves del archivo properties
	//@ Value("${square.slide}") // Para definir un valor por si no coge el valor del archivo properties: @Value("${square.slide:1}")
	private double side;
	
	@Override
	public double calcularArea() {
		
		return side*side;
	}

}
