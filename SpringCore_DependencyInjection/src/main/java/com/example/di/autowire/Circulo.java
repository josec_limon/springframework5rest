package com.example.di.autowire;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Circulo implements Figure{

	//@Value("2.5")
	//@Value("${circle.radius}")
	private double radius;
	
	@Override
	public double calcularArea() {

		return Math.PI * Math.pow(radius, 2);
	}

}
