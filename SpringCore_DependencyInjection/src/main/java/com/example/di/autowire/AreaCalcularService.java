package com.example.di.autowire;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AreaCalcularService {
	
	@Autowired
	private List<Figure> figures;
	public double calcAreas() {

		double area=0;
		for(Figure fig : figures) {
			area+=fig.calcularArea();
		}
		
		return area;
	}
}
