package com.example.di.qualifiers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class Perro extends Animal{

	public Perro(@Value("Blue") String nombre, @Value("2") int edad) {
		super(nombre, edad);
		// TODO Auto-generated constructor stub
	}

	
}
