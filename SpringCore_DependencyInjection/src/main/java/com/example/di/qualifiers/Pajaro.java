package com.example.di.qualifiers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("pajarito")
public class Pajaro extends Animal implements Volador{

	public Pajaro(@Value("PajaroLoco") String nombre, @Value("3") int edad) {
		super(nombre, edad);
		// TODO Auto-generated constructor stub
	}


	private static final Logger log = LoggerFactory.getLogger(Pajaro.class);
	
	
	@Override
	public void volar() {
		log.info("Soy un pájaro y estoy volando");
		
	}

}
