package com.example.di.qualifiers;

import org.springframework.stereotype.Component;

@Component
public interface Volador {

	public void volar();
}
