package com.example.di.atributo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Coche {

	//@Value("VW")	// Dependency Injection por atributo
	private String marca;
	//@Value("1981")	// Dependency Injection por atributo
	private int modelo;
	
	//@Autowired
	private Motor motor;
	
	public Coche() {
		// TODO Auto-generated constructor stub
	}
	
	// Dependency Injection por constructor
	/*@Autowired
	public Coche(String marca, int modelo, Motor motor) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.motor = motor;
	}

	public String getMarca() {
		return marca;
	}*/

	@Value("VW") 
	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getModelo() {
		return modelo;
	}

	@Value("1981")
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	public Motor getMotor() {
		return motor;
	}

	@Autowired
	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	@Override
	public String toString() {
		return "Coche [marca=" + marca + ", modelo=" + modelo + ", motor=" + motor + "]";
	}
		
		
}
