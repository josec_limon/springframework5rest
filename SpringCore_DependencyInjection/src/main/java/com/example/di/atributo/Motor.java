package com.example.di.atributo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Motor {

	//@Value("Xl1")		// Dependency Injection por atributo
	private String marca;
	
	//@Value("1982")
	private int modelo;
		
	// Dependency Injection por constructor
	
	// Tenemos que quitar el constructor vacio 
	//public Motor() {}
	
	/*public Motor(@Value("Xl1") String marca, @Value("1982") int modelo) {
		super();
		this.marca = marca;
		this.modelo = modelo;
	}*/
	
	// Dependency Injection por setters
	public Motor() {}
	
	public String getMarca() {
		return marca;
	}

	@Value("Xl1")	
	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getModelo() {
		return modelo;
	}

	@Value("1982")
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	@Override
	public String toString() {
		return "Motor [marca=" + marca + ", modelo=" + modelo + "]";
	}



	
	
}
