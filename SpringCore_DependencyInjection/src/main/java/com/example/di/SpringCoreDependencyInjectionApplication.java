package com.example.di;

import java.beans.Expression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Service;

import com.example.di.aop.TargetObject;
import com.example.di.atributo.Coche;
import com.example.di.atributo.Motor;
import com.example.di.autowire.AreaCalcularService;
import com.example.di.lifecycle.ExplicitBean;
import com.example.di.lifecycle.LifeCycleBean;
import com.example.di.profiles.EnvironmentService;
import com.example.di.qualifiers.Animal;
import com.example.di.qualifiers.Avion;
import com.example.di.qualifiers.Nido;
import com.example.di.qualifiers.Pajaro;
import com.example.di.qualifiers.Perro;
import com.example.di.scopes.EjemploScopeService;

// Con @SpringBootApplication simplificamos las siguientes etiquetas: 
// @Configuration
// @ComponentScan
// @EnableAutoConfiguration
@SpringBootApplication
public class SpringCoreDependencyInjectionApplication {

	
	private static final Logger log = LoggerFactory.getLogger(SpringCoreDependencyInjectionApplication.class);

	public static void main(String[] args) {
		
		//Crear un objeto mediante Dependency Injection
		ConfigurableApplicationContext context = SpringApplication.run(SpringCoreDependencyInjectionApplication.class, args);
	
		//Coche coche=context.getBean(Coche.class);
		//System.out.println(coche);
		// Va a crear un objeto Coche y un objeto Motor utilizando los constructores
		
		/*Motor motor= new Motor("Xl1",1981);
		Coche coche = new Coche("VW", 1986,motor);
		
		System.out.println(coche);
		*/
	
		//Probando los qualifiers
		//Perro miPerro=context.getBean(Perro.class);
		//log.info("Objeto perro {" + miPerro.getNombre() + "}");
		
		//Pajaro miPajaro=context.getBean(Pajaro.class);
		//log.info("Objeto pajaro {" + miPajaro.getNombre() + "}");
	
		
		//Avion miBoing=context.getBean(Avion.class);
		//miBoing.volar();
		
		// Defino el bean que quiero utilizar con @Component("pajarito") en la clase Pajaro
		//Animal miAnimal=context.getBean("pajarito", Animal.class);
		//log.info("Objeto animal { " + miAnimal.getNombre() + " - " + miAnimal.getEdad() +"} ");
		
		// Otra opcion es definir el bean que utilizaremos es con la etiqueta @Autowired y @Qualifier("pajarito") en la clase Nido
		// Otra manera es utilizar @Primary para definir el bean por defecto que queremos utilizar... 
		//Nido miNido=context.getBean(Nido.class);
		//miNido.imprimir();
		
		//Establecer un perfil como activo. Ver txt con las distintas maneras.
		//EnvironmentService environmentServ = context.getBean(EnvironmentService.class);
		//log.info("Active environment: "  + environmentServ.getEnvironment());
		
		//Scopes
		//EjemploScopeService ejemploScopeServ1=context.getBean(EjemploScopeService.class); 
		//EjemploScopeService ejemploScopeServ2=context.getBean(EjemploScopeService.class); 
		
		//log.info("Are beans equals (" + ejemploScopeServ1.equals(ejemploScopeServ2));
		//log.info("Are beans == " + (ejemploScopeServ1 == ejemploScopeServ2));
	
		//Declaración del beans de forma explicita
		//String nombreAplicacion = context.getBean(String.class);
		//log.info("Nombre aplicacion: {}",nombreAplicacion );
		
		//Autowired
		//AreaCalcularService calculator=context.getBean(AreaCalcularService.class);
		//log.info("Areas total: {}",calculator.calcAreas() );
		
		// Evaluacion de expresiones
		//ExpressionParser expParser = new SpelExpressionParser();
		//org.springframework.expression.Expression expression =expParser.parseExpression("10 + 20");
		//log.info("Expression result: {}", expression.getValue());	
		
		
		//LyfeCycle 
		//LifeCycleBean lifeBean=context.getBean(LifeCycleBean.class);
		
		//AOP
		TargetObject tarObj = context.getBean(TargetObject.class);
		tarObj.hello("Hello World");
		//tarObj.foo();
	}
	
	//Declaración del bean de forma explicita
	@Bean
	public String getApplicationBean(){
		return "DEV14 rules";
	}

	//Definir BEAN de forma explicita
	//@Bean(initMethod="init", destroyMethod="destroy")
	//public ExplicitBean getBean(){
	//	return new ExplicitBean();
	//}
}
