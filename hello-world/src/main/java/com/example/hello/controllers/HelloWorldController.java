package com.example.hello.controllers;

import org.springframework.boot.SpringApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController	 //Define que la clase es un controlador
@RequestMapping("/helloworld")		//Define el punto de inicio de este codigo.
public class HelloWorldController {

	@GetMapping  //Accedo al método por GET
	public ResponseEntity<String> helloWorld(){
		return new ResponseEntity<>("Hello World", HttpStatus.OK);
	}
}
