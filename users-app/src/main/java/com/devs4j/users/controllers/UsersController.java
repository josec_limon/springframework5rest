package com.devs4j.users.controllers;

import java.util.List;

import com.devs4j.users.services.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController	//Stereotype
@RequestMapping("/api/users")
public class UsersController<User> {
	
	@Autowired
	private userService userService;
	
	@GetMapping
	public ResponseEntity<List<User>> getUsers(){
		ResponseEntity<List<User>> respuesta = new ResponseEntity<List<User>>((List<User>) userService.getUsers(),HttpStatus.OK);
		return respuesta;	
	}
	
	@GetMapping(value="/{username}")
	public ResponseEntity<User> getUserByUsername(@PathVariable("username") String username){
		ResponseEntity<User> respuesta = new ResponseEntity<User>((User) userService.getUserByName(username),HttpStatus.OK);
		return respuesta;	
	}
}
