package com.devs4j.users.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import com.devs4j.users.models.User;
import com.github.javafaker.Faker;

@Service
public class userService {
	
	@Autowired
	private Faker faker;
	private List<User> listado = new ArrayList<User>();
	
	@PostConstruct
	public void init() {
		//Inicializar listado de users
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));
		listado.add(new User(faker.funnyName().name(),faker.pokemon().name(), faker.crypto().md5()));

	}
	
	public List<User> getUsers() {
		return listado;
	}
	
	public User getUserByName(String username) {
		User usuarioEncontrado = new User();
		usuarioEncontrado=listado.stream().filter(u -> u.getUserName().equals(username))
						  .findAny().orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND, 
								  							String.format("Usuario no encontrado", username)));
		
		return usuarioEncontrado;
	}
}
